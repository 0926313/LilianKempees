--- 
title: De eerste dag
date: 2017-09-04
---
De eerste **lesdag**, ik vind het spannend en weet niet goed wat er gaat gebeuren. 
 
In de studio hebben we goed kunnen werken, er zijn (nog) niet veel vaardigheden nodig met programma’s, zoals Photoshop, Indesign en Illustrator. Nu gaat het vooral om onderzoek doen naar de doelgroep, en een concept bedenken. Dit gaan we verwerken in een paper prototype, hoe dit er uit gaat zien weet ik nog niet. Toen we in ons team aan de opdracht gingen werken, hebben we als eerste gebrainstormd over wat mensen onderling en mensen met Rotterdam zou verbinden. Zelf heb ik ook gekeken naar wat mij met Rotterdam heeft verbonden, en mijn ervaring met de Rotterdamtour uit de introductieweek. We kwamen al snel tot de conclusie dat we een tour door Rotterdam moeten doen, met op verschillende locaties verschillende opdrachten die mensen verbindt. We hebben gepraat over hoe de opdrachten er dan uit moeten zien, en liepen al snel tegen een gebrek aan informatie aan over de mogelijkheden wat betreft het gebruik van peercoaches en andere onderwerpen. Soms liepen we wat te hard van stapel, en misten we onderzoek. Er is wel een rauw concept gevormd van wat we willen dat er in het spel gebeurd, deze heb ik uitgeschreven in mijn dummy. Ook hebben we afspraken gemaakt wat woensdag af moet zijn, en hoe we vooralsnog de planning willen hebben voor het inleveren van het eerste prototype. 
Wat ik merkte is dat niet alleen ik, maar ook de anderen in mijn team veel dingen nog onduidelijk vinden aan de opdracht, en waar onze grenzen liggen m.b.t. de opdracht. Ook over andere zaken, zoals de keuzevakken en workshops is veel onduidelijkheid. Toch fijn om te weten dat ik niet de enige ben hierin.
Ik wil alvast kwartaaldoelen opstellen, deze zijn nog onder voorbehoud:

 - Photoshop, Illustrator of Indesign aanleren (cursus of  via internet)
 - HTML & CSS aanleren (Via boek en/of internet)
 - Een goede start maken, leergierig zijn en op tijd beginnen voor het tentamen